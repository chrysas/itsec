#Choose xelatex as the default builder of pdfs, don't stop for errors, use synctex
$pdflatex = 'pdflatex -interaction=nonstopmode -synctex=1 -shell-escape %O %S';
# .bbl files assumed to be regeneratable, safe as long as the .bib file is available
$bibtex_use = 2;
# User biber instead of bibtex
$biber = 'biber --debug %O %S';
# Default pdf viewer
#Linux
$pdf_previewer = 'start evince %O %S';
#$pdf_previewer = 'open %O %S';
#MacOS
#$pdf_previewer = 'open -a /Applications/Skim.app %O %S';

push @generated_exts, 'glo', 'gls', 'glg';
push @generated_exts, 'acn', 'acr', 'alg';
push @generated_exts, 'sum', 'verbose.sum';
push @generated_exts, 'tdo', 'xdy', 'ist', 'snm', 'nav', 'blg';

# Extra file extensions to clean when latexmk -c or latexmk -C is used
$clean_ext = '%R.run.xml %R.synctex.gz %R.sum %R.verbose.sum %R.tdo *.blg %R.nav %R.snm %R.ist %R.xdy';

# Use pdflatex recorder functionality
$recorder = 0;

#$compiling_command = 'terminal-notifier -message Compiling %R COMPILING -title Latexmk -sound default -sender com.sublimetext.3';
#$failure_command = 'terminal-notifier -message Compiling %R FAILED -title Latexmk -sound default -sender com.sublimetext.3';
#$success_command = 'terminal-notifier -message Compiling %R SUCCEEDED -title Latexmk -sound default -sender com.sublimetext.3';

#$out_dir = 'out';

#Build with pdflatex
$pdf_mode = 1;
$preview_continuous_mode = 1;

add_cus_dep('glo', 'gls', 0, 'run_makeglossaries');
add_cus_dep('acn', 'acr', 0, 'run_makeglossaries');

sub run_makeglossaries {
  if ( $silent ) {
    system "makeglossaries -q '$_[0]'";
  }
  else {
    system "makeglossaries '$_[0]'";
  };
}


